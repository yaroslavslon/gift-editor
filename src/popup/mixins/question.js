export const question = {
  props: {
    serialNumber: {
      type: Number,
      required: true,
    },
    questionType: {
      type: String,
      required: false,
    },
    gift: {
      required: true,
    },
  },
  data: function() {
    return {
      title: '',
      question: '',
      rightAnswer: null,
      visible: false,
    };
  },
  methods: {
    updateTitle: function(title) {
      this.title = title;
    },
    updateQuestion: function(question) {
      this.question = question;
    },
    showOrHide: function() {
      this.visible = !this.visible;
    },
    deleteQuestion: function() {
      this.$parent.delQuestion(this.serialNumber - 1);
    },
  },
  mounted: function() {
    if (this.gift !== null) {
      this.title = this.gift.title ? this.gift.title : '';
      this.question = this.gift.stem.text;
    }
  },
  destroyed: function() {
    console.info('Deleted question with number: ', this.serialNumber);
  },
};
